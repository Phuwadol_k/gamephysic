﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class connon : MonoBehaviour
{
    [SerializeField]
    private GameObject Target;

    [SerializeField]
    private float delay;

    [SerializeField]
    private float Startdelay;
    [SerializeField]
    private float Speed;
    [SerializeField]
    private float Mass;

    [SerializeField]
    private float radius;
    [SerializeField]
    private float deadtime=2;

    public Material mat;
    // Start is called before the first frame update
    void Start()
    {
        
            delay = Startdelay;
        
    }

    // Update is called once per frame
    void Update()
    {
        if(delay<=0)
        {

            delay = Startdelay;
            shoot();


        }
        else
        {
            delay -= Time.deltaTime;
        }
    }
    void shoot()
    {
        GameObject go = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        go.transform.localScale = new Vector3(radius, radius, radius);

        SphereCollider coli = go.GetComponent<SphereCollider>();
        coli.isTrigger = false;
        Rigidbody Rb = go.AddComponent<Rigidbody>();
        Rb.useGravity = true;
        Rb.mass = Mass;
        go.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + 0.5f, this.transform.position.z);
        go.gameObject.GetComponent<MeshRenderer>().material = mat;
        Rb.AddForce((Target.transform.position - transform.position).normalized *Speed, ForceMode.Impulse);


        Destroy(go, deadtime);


    }
}
