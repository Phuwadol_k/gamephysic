﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class Gamemananger : MonoBehaviour
{
    public Vector3 savepoint;
    public static Gamemananger instant;
    public GameObject player;
    public bool isdead = false;
    public bool pause = false;
    public GameObject panalPause;
    public GameObject panelGameOver;
    public int part = 10;
    [SerializeField] Button _RestartButton;
    [SerializeField] Button _RestartButton2;
    [SerializeField] Button _exitButton;
    // Start is called before the first frame update
    private void Awake()
    {
        if (instant == null)
        {
            instant = this;
            DontDestroyOnLoad(gameObject);

        }
        else
        {
            Destroy(gameObject);
        }
    }
    void Start()
    {
        panalPause.SetActive(false);
        panelGameOver.SetActive(false);
        SetupDelegate();
    }

    // Update is called once per frame
    void Update()
    {
     if(player==null)
        {
            player = GameObject.FindGameObjectWithTag("Player");
        }
        if(player.transform.position.y<-20)
        {
            isdead = true;
        }
        if(Input.GetKeyDown(KeyCode.Escape))
        {   if (!pause)
            {
                panalPause.SetActive(true);
                pause = true;
            }

            else
            {
                panalPause.SetActive(false);
                pause = false;
            }
                
        }
        if(isdead)
        {
            panelGameOver.SetActive(true);
        }
        if(part<=0)
        {
            isdead = true;
        }
    }




void SetupDelegate()
{
        _RestartButton.onClick.AddListener(delegate { StartButtonClick(_RestartButton); });
        _RestartButton2.onClick.AddListener(delegate { StartButtonClick(_RestartButton2); });
        _exitButton.onClick.AddListener(delegate { ExitButtonCilck(_exitButton); });

  
}
public void StartButtonClick(Button button) { SceneManager.LoadScene("Lv1");
        panalPause.SetActive(false);
        panelGameOver.SetActive(false);
        pause = false;
        isdead = false;
        part = 8;
    }

    public void ExitButtonCilck(Button button)
    {
        SceneManager.LoadScene("Mainmenu");
        panalPause.SetActive(false);
        panelGameOver.SetActive(false);
        pause = false;
        isdead = false;
        part = 8;
    }


}
