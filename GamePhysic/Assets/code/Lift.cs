﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lift : MonoBehaviour
{
    private Vector3 targetPosition;
   private ConfigurableJoint cf;
    private bool on = false;
    private void Start()
    {
       cf=GetComponent<ConfigurableJoint>();
        targetPosition.x = -9;
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Player"))
        {


            on = true;



        }
    }

    private void Update()
    {if(on)
        {
         if(cf.targetPosition.x<15)
                cf.targetPosition = new Vector3(targetPosition.x++,cf.targetPosition.y ,cf.targetPosition.z);
        }
        
    }
}
