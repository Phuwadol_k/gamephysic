﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class part : MonoBehaviour
{
    private bool isact = false;
    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            if (!isact)
            {
                Gamemananger.instant.part -= 1;
                isact = true;
            }
            
        }
    }
}
