﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TorqueSettings 
{
    public bool isGlobal = true;
    public Vector3 torqueAxis;
    public float amountOfTorque = 100;
    public ForceMode forceMode = ForceMode.Impulse;
}
