﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MainMenuManager : MonoBehaviour
{


    [SerializeField] Button _startButton;


    [SerializeField] Button _exitButton;





    private void Start()
    {
    //    this.audiosourceButtomUI = this.gameObject.AddComponent<AudioSource>();
     // this.audiosourceButtomUI.outputAudioMixerGroup = SoundManager.Instance.Mixer.FindMatchingGroups("MasterSFXVolume")[0];
 SetupDelegate();
     //
     // if (!SoundManager.Instance.BGMSource.isPlaying)
     //     SoundManager.Instance.BGMSource.Play();


    }
    void SetupDelegate()
    {
        _startButton.onClick.AddListener(delegate { StartButtonClick(_startButton); });
     
     
        _exitButton.onClick.AddListener(delegate { ExitButtonCilck(_exitButton); });

      //  _SoundTestButton.onClick.AddListener(delegate { SoundTestButtonClick(_SoundTestButton); });
    }
 
    public void StartButtonClick(Button button) { SceneManager.LoadScene("Lv1"); }

    public void OptionsButtonClick(Button button)
    {
      // if (!GameApplicationManager.Instance.IsOptionMenuActive)
      // {
         SceneManager.LoadScene("SceneOptions");
      //     GameApplicationManager.Instance.IsOptionMenuActive = true;
      // }

    }

 

    public void ExitButtonCilck(Button button)
    {
        Application.Quit();
    }


}



