﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Maximum : MonoBehaviour
{
    Rigidbody _rigibody;
    [SerializeField]
    [Tooltip("Degrees per second")]
    float _maximumAngularVelocity = 2.0f;
    // Start is called before the first frame update
    void Start()
    {
        Rigidbody rb = this.gameObject.GetComponent<Rigidbody>();
        _rigibody = rb;
        if (rb != null)
        {
            _rigibody.maxAngularVelocity = _maximumAngularVelocity * Mathf.Deg2Rad;
        }
    }

    // Update is called once per frame
 public void SetMaximumAngularSpeedLimit(float val)
    {
        _maximumAngularVelocity = val * Mathf.Deg2Rad;
    }
}
